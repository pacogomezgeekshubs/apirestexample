package com.geekshubs.apirestdemo.controller;

import com.geekshubs.apirestdemo.pojo.EjemploMsg;
import com.geekshubs.apirestdemo.pojo.Todo;
import com.geekshubs.apirestdemo.pojo.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ExampleRestController {

    @Autowired
    private TodoService todoObject;

    @GetMapping("/v1/welcome")
    public String welcome(){
        return "Hello World";
    }

    @GetMapping("/v1/textos")
    public EjemploMsg sacarTexto(){
        return new EjemploMsg("Hola Paco");
    }

    @GetMapping("/v1/textos/{texto}")
    public EjemploMsg sacarTexto(@PathVariable String texto){
        return new EjemploMsg("Hola "+texto);
    }

    @GetMapping("/v1/users/{name}/todos")
    public List<Todo> retrieveTodos(@PathVariable String name) {
        return todoObject.retrieveTodos(name);
    }

    @GetMapping("/v1/users/{id}")
    public Todo retrieveUsersId(@PathVariable String id) {
        return todoObject.retrieveTodoId(Integer.parseInt(id));
   }

    @GetMapping("/v1/users/{name}/todos/{id}")
    public Todo retrieveUsersId(@PathVariable String name,@PathVariable String id) {
        return todoObject.retrieveTodos(name,Integer.parseInt(id));
    }

    @PostMapping("/v1/users/{name}/todos")
    ResponseEntity<Todo> add(@PathVariable String name,@RequestBody Todo todo) {
        System.out.println(todo);
        Todo createdTodo= todoObject.addTodo(name,todo.getDesc(),todo.getTargetDate(),todo.getDone());
        return new ResponseEntity<Todo>(createdTodo, HttpStatus.OK);
    }



}
