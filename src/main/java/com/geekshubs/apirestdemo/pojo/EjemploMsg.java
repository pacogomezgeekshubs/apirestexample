package com.geekshubs.apirestdemo.pojo;

public class EjemploMsg {
    private String texto;

    public EjemploMsg(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
