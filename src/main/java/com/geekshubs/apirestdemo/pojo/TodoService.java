package com.geekshubs.apirestdemo.pojo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private ArrayList<Todo> todos = new ArrayList<Todo>();

    private int todoCount;

    public TodoService(){
        todos.add(new Todo(1, "Sergio", "Learn Spring MVC", new Date(), false));
        todos.add(new Todo(2, "Paco", "Learn Struts", new Date(), false));
        todos.add(new Todo(3, "Paco", "Search Date to LocalDate casting", new Date(), false));
        todos.add(new Todo(4, "Jesus", "Comprar portatil nuevo", new Date(), false));
        todoCount=todos.size();
    }

    public List<Todo> retrieveTodos(String name){
        return todos.stream().filter(t->t.getUser().compareTo(name)==0).collect(Collectors.toList());
    }
    public Todo retrieveTodos(String name,int id){
        return todos.stream().filter(t->t.getUser().compareTo(name)==0).filter(t->t.getId()==id).collect(Collectors.toList()).get(0);
    }

    public Todo retrieveTodoId(int id){
        return todos.stream().filter(t->t.getId()==id).collect(Collectors.toList()).get(0);
    }

    public Todo addTodo(String name, String desc, Date targetDate,
                        boolean isDone) {
        todos.add(new Todo(++todoCount, name, desc, targetDate, isDone));
        return todos.get(todoCount-1);
    }

}
